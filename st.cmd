#!/usr/bin/env iocsh.bash
require(modbus)
require(s7plc)
require(calc)
epicsEnvSet(loki-chpsy3-sc-ioc-001_VERSION,"plcfactory")
iocshLoad("$(essioc_DIR)/essioc.iocsh")
iocshLoad("./loki-chpsy3-sc-ioc-001.iocsh","IPADDR=172.30.235.36,RECVTIMEOUT=3000")

#Load alarms database
epicsEnvSet(P,"LOKI-ChpSy3:")
epicsEnvSet(R1,"Chop-SFOC-101:")
epicsEnvSet(R2,"Chop-SFOC-102:")
#epicsEnvSet(R3,"Chop-NA-101:")
#epicsEnvSet(R4,"Chop-NA-102:")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R1)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R2)")
#dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R3)")
#dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R4)")

iocInit()
#EOF
